# Exploring the Effects of Segmentation on Semi-Structured Interview Data with Epistemic Network Analysis
S. Zörgő, Z. Swiecki, A.R. Ruis

Quantitative ethnographic models are typically constructed using qualitative data that has been segmented and coded. While there exist methodological studies that have investigated the effects of changes in coding on model features, the effects of segmentation have received less attention. Our aim was to examine, using a dataset comprised of narratives from semi-structured interviews, the effects of different segmentation decisions on population- and individual-level model features via epistemic network analysis. We found that while segmentation choices may not affect model features overall, the effects on some individual networks can be substantial. This study demonstrates a novel method for exploring and quantifying the impact of segmentation choices on model features.

Preprint URL: https://psyarxiv.com/sepbr
APA citation: Zörgő, S., Swiecki, Z., & Ruis, A. (2020, October 26). Exploring the Effects of Segmentation on Semi-Structured Interview Data with Epistemic Network Analysis. https://doi.org/10.31234/osf.io/sepbr